# SMS Spam Detection Using Machine Learning

This project is used a starting point for the course [*Release Engineering for Machine Learning Applications* (REMLA)] taught at the Delft University of Technology by [Prof. Luís Cruz] and [Prof. Sebastian Proksch].

The codebase was originally adapted from: https://github.com/rohan8594/SMS-Spam-Detection

## Instructions for Compiling

a) Clone repo.

```
$ git clone https://github.com/rohan8594/SMS-Spam-Detection.git
$ cd SMS-Spam-Detection
```

b) Install dependencies.

```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

c) Run various scripts

```
$ python src/read_data.py
$ python src/text_preprocessing.py
$ python src/text_classification.py
```

d) Serve the model as a REST API

```
$ python src/serve_model.py
```

You can test the API using the following:

```
curl -X POST "http://127.0.0.1:8080/predict" -H  "accept: application/json" -d "{sms: hello world!}"
```

Alternatively, you can access the UI using your browser: http://127.0.0.1:8080/apidocs

[*Release Engineering for Machine Learning Applications* (REMLA)]: https://se.ewi.tudelft.nl/remla/ 
[Prof. Luís Cruz]: https://luiscruz.github.io/
[Prof. Sebastian Proksch]: https://proks.ch/


## Instructions for using gitlab-ci pipeline
In the .gitlab-ci.yml file, there are two stages -- test stage and publish stage.

### Test Stage

In the test stage, the dslinter will be run to evaluate the code quality. There are three main scripts to generate the dslinter code quality report. You can use each one of them or use three of them according to your need.

```
script1:
pylint --load-plugins=dslinter --disable=all --enable=dataframe,nan,hyperparameters,import,data-leakage,controlling-randomness,excessive-hyperparameter-precision,pca-scaler --exit-zero --output-format=text $(find -type f -name "*.py" ! -path "**/.venv/**") | tee /tmp/pylint.txt
```
This line is used to save the code quality score to `/tmp/pylint.txt`. The saved score can be used to output to the screen to show the information and make a badge.

```
script2:
pylint --load-plugins=dslinter --disable=all --enable=dataframe,nan,hyperparameters,import,data-leakage,controlling-randomness,excessive-hyperparameter-precision,pca-scaler --exit-zero --output-format=pylint_gitlab.GitlabPagesHtmlReporter $(find -type f -name "*.py" ! -path "**/.venv/**") > public/lint/index.html
```
This script can be used to generate a webpage to show the code quality violation as the following picture shows. This webpage can be seen in CI/CD -> Pipelines -> Stages -> Browse -> public -> lint -> index.html .

![](https://i.imgur.com/TYtcPd0.png)


```
script3:
pylint --load-plugins=dslinter --disable=all --enable=dataframe,nan,hyperparameters,import,data-leakage,controlling-randomness,excessive-hyperparameter-precision,pca-scaler --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter $(find -type f -name "*.py" ! -path "**/.venv/**") > codeclimate.json
```
This line is used to save a `codeclimate.json`, which can be used as a code quality report. With this line, when you create a merge request, you can see the code quality change in the merge request widget area, as the following picture shows.
![](https://i.imgur.com/ROmFW4b.png)

If you go into the merge request’s diff view, you can see the indicators next to lines with code quality violations, as the following picture shows. This function can only be used in gitlab ultimate version now.
![](https://i.imgur.com/fA4dFte.png)

If you want to make the test stage fail when the code quality score is under a threshold, you can add a new script as follows.
```
script4:
pylint --load-plugins=dslinter --disable=all --enable=dataframe,nan,hyperparameters,import,data-leakage,controlling-randomness,excessive-hyperparameter-precision,pca-scaler --fail-under=$THRESHOLD
```
For example, if you want the test stage to fail under 8, you can set `--fail-under=8`.


### Publish Stage

If you add a version tag to the commit, the publish stage will be triggered. In this stage, a new version ML model artifact will be published to the GitLab registry. 

However, if you add the `--fail-under=$THRESHOLD` script in the .gitlab-ci.yml and your test stage fails, the publish stage will not be triggered even if you add a tag to it.

### Links to the repositories
mymodels: https://gitlab.com/remla2/mymodels
dslinter: https://github.com/Hynn01/dslinter


